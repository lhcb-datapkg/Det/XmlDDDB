#!/usr/bin/perl
use Getopt::Long;
&GetOptions('f=s',\$f,'nsta=s',\$nsta,'fsta=s',\$fsta,'nreg=s',\$nreg,'ngap=s',\$ngap,'freg=s',\$freg,'tr=s',\$tr,'exe=s',\$exe,'help',\$help);

$StrFilName = "DefStructure.xml";
$StrFilName = $f if($f);

$StaFilName = "StaStructure.xml";
$StaFilName = $fsta if($fsta);

$RegFilName = "RegStructure.xml";
$RegFilName = $freg if($freg);

$stations = 5;
$stations = $nsta if($nsta);

$regions = 4;
$regions = $nreg if($nreg);

$ngaps = 4;
$ngaps = $ngap if($ngap);

$myDir = "GapDir";

usage(0) if($help);

my $idxs = 1;

open OUTF,">$StrFilName";
print OUTF "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print OUTF "<!DOCTYPE DDDB SYSTEM \"../DTD/structure.dtd\" []>\n";
print OUTF "<DDDB>\n";
print OUTF "  <!-- File Generated with perl script -->\n";
print OUTF "  <detelem name=\"Muon\" classID=\"11009\">\n";
print OUTF "    <geometryinfo lvname=\"/dd/Geometry/DownstreamRegion/Muon/lvMuon\"\n";
print OUTF "		  npath=\"MuonSubsystem\" \n";
print OUTF "                  support=\"/dd/Structure/LHCb/DownstreamRegion\"/>\n";
while($idxs <= $stations) {
  print OUTF "    <detelemref href =\"$StaFilName#M$idxs\" />\n";
  $idxs++;
}
print OUTF "  </detelem>\n";
print OUTF "</DDDB>\n";
close OUTF;

insert_station($stations,$StaFilName,$regions,$RegFilName);

$useSpec = 0;
if($useSpec) {
  insert_regions($stations,$regions,$RegFilName);
}

dump_regionStr($stations,$regions,$RegFilName);

$idxs = 1;
while ($idxs <= $stations) {
  $idxr = 1;
  while ($idxr <= $regions) {
    my @myNcha = (0,12,24,48,192);
    my $mytmpChm = 1;
    while ($mytmpChm <= $myNcha[$idxr]) {
      if ($mytmpChm <10) {
	$chamName = "Cham00${mytmpChm}";
      } elsif ($mytmpChm <100) {
	$chamName = "Cham0${mytmpChm}";
      } else {
	$chamName = "Cham${mytmpChm}";
      }
      $ngaps = 4;
      $ngaps = 2 if($idxs == 1);
      dump_gapStr("M$idxs","R$idxr","$chamName",$ngaps);

      $mytmpChm++;
    }
    $idxr++;
  }
  $idxs++;
}

#Old routine
#create_layout($stations,$StaFilName,$regions,$RegFilName);

print "Created files: $StrFilName, $StaFilName and ${myDir}/* \n";

sub insert_station {
  my($num,$fname,$nreg,$rname) = @_;
  open OUTF,">$fname";
  print OUTF "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  print OUTF "<!DOCTYPE DDDB SYSTEM \"../DTD/structure.dtd\" []>\n";
  print OUTF "<DDDB>\n";
  $idxs = 1;
  while($idxs <= $num) {
    $backs = "";
    $backs = "pvMuonBack/"    if($idxs != 1);
    print OUTF "  <detelem name =\"M$idxs\" >\n";
    print OUTF "    <geometryinfo lvname=\"/dd/Geometry/DownstreamRegion/Muon/StatPos/lvM$idxs\"\n";
    print OUTF "		  npath=\"${backs}pvM$idxs\"\n";
    print OUTF "		  support=\"/dd/Structure/LHCb/DownstreamRegion/Muon\"/> \n";
    $idxr = 1;
    while($idxr <= $nreg) {
      print OUTF "    <detelemref href =\"${myDir}/M${idxs}R${idxr}\@${rname}#R$idxr\" />\n";
      $idxr++;
    }
    print OUTF "  </detelem>\n";
    $idxs++;


  }
  print OUTF "</DDDB>\n";

  close OUTF;
}

sub dump_regionStr {
  my($nsta,$nreg,$rname) = @_;
  if($rname =~ /(.*)\.xml/) {
    $modname = $1;
  }
  my @myNcha = (0,12,24,48,192);
  my @grid0 = (1,11,8,10);
  my @grid1 = (2,4,12,13);
  my @grid2 = (3,5,12,13);
  my @grid3 = (6,7,9,14);
  my @grid4 = (6,7,9,14);

  $idxs = 1;
  while ($idxs <= $nsta) {

    $idxr = 1;
    while ($idxr <= $nreg) {
    
      if(! -e "${myDir}") {
        print" mkdir ${myDir}\n";
        system(" mkdir ${myDir}");
      }
    
      print "Chambers per region:: $myNcha[$idxr]\n";
      open OUTF,">${myDir}/M${idxs}R${idxr}\@${rname}";
      print OUTF "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
      print OUTF "<!DOCTYPE DDDB SYSTEM \"../../DTD/structure.dtd\" []>\n";
      print OUTF "\n";
      print OUTF "<!-- ***************************************************************** -->\n";
      print OUTF "<!-- *  Template for a region with chambers.                         * -->\n";
      print OUTF "<!-- ***************************************************************** -->\n";
      print OUTF "\n";
      print OUTF "\n";
      print OUTF "<DDDB>\n";
      print OUTF "  <detelem name = \"R${idxr}\" classID=\"11005\">\n";
      print OUTF "    <author> A.Sarti </author>\n";
      print OUTF "    <version> 1.0 </version>\n";
      print OUTF "    <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/M${idxs}ChamPos/lvR${idxr}\"\n";
      print OUTF "                 support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M${idxs}\"\n";
      print OUTF "                   npath = \"pvM${idxs}R${idxr}\" />\n";
      my $mytmpChm = 1;
      while ($mytmpChm <= $myNcha[$idxr]) {
	if ($mytmpChm <10) {
	  print OUTF "    <detelemref href =\"#Cham00${mytmpChm}\" />\n";
	} elsif ($mytmpChm <100) {
	  print OUTF "    <detelemref href =\"#Cham0${mytmpChm}\" />\n";
 	} else {
	  print OUTF "    <detelemref href =\"#Cham${mytmpChm}\" />\n";
	}
	$mytmpChm++;
      }
      print OUTF "  </detelem>\n";
      print OUTF "\n";

      #Directories to hold gaps 
      if(! -e "${myDir}/GapStructureM${idxs}R${idxr}") {
	print" mkdir ${myDir}/GapStructureM${idxs}R${idxr}\n";
	system(" mkdir ${myDir}/GapStructureM${idxs}R${idxr}");
      }

      if(${idxs} == 1) {
	$grNum =  $grid0[${idxr}-1];
      } elsif(${idxs} == 2) {
	$grNum =  $grid1[${idxr}-1];
      } elsif(${idxs} == 3) {
	$grNum =  $grid2[${idxr}-1];
      } elsif(${idxs} == 4) {
	$grNum =  $grid3[${idxr}-1];
      } else {
	$grNum =  $grid4[${idxr}-1];
      }

      my $mytmpChm = 1;
      while ($mytmpChm <= $myNcha[$idxr]) {

	if ($mytmpChm <10) {
	  $chamName = "Cham00${mytmpChm}";
	} elsif ($mytmpChm <100) {
	  $chamName = "Cham0${mytmpChm}";
	} else {
	  $chamName = "Cham${mytmpChm}";
	}

#	print OUTF "  <detelem classID = \"11006\" name = \"${chamName}\">\n";
	print OUTF "  <detelem name = \"${chamName}\" classID=\"11006\">\n";
	print OUTF "    <userParameter name=\"Grid\" type= \"string\"> G${grNum} </userParameter>\n";
	print OUTF "    <conditioninfo name=\"G${grNum}\" condition=\"/dd/Conditions/ReadoutConf/Muon/Grid/G${grNum}\" />\n";
	print OUTF "    <geometryinfo lvname  = \"/dd/Geometry/DownstreamRegion/Muon/M${idxs}R${idxr}Cham/lvM${idxs}R${idxr}Cham\"\n";
	print OUTF "	            support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M${idxs}/R${idxr}\"\n";
	print OUTF "                    npath   = \"pvM${idxs}R${idxr}${chamName}\" />\n";
	$useSpec = 0;
	if ($useSpec) {
	  print OUTF "    <specific>\n";
	  print OUTF "      <GasGap chamber=\"${mytmpChm}\" \n";
	  print OUTF "              Number=\"&gNum;\" \n";
	  print OUTF "              offset=\"3\" \n";
	  print OUTF "              logvol=\"/dd/Geometry/DownstreamRegion/Muon/M${idxs}R${idxr}Cham/lvGasGapLayer5\" \n";
	  print OUTF "              support=\"/dd/Structure/LHCb/DownstreamRegion/Muon/M${idxs}/R${idxr}\"/>\n";
	  print OUTF "    </specific>\n";
	} else {

	  $ngaps = 4;
	  $ngaps = 2 if($idxs == 1);
	  $idxg = 1;

	  while ($idxg <= $ngaps) {
	    print OUTF "    <detelemref href =\"GapStructureM${idxs}R${idxr}/${chamName}\@GapStructureM${idxs}R${idxr}.xml#Gap$idxg\" />\n";
	    $idxg++;
	  }

#	  dump_gapStr("M$idxs","R$idxr","$chamName",$ngaps);

	}
	print OUTF "  </detelem>\n";
	$mytmpChm++;
      }
      print OUTF "</DDDB>\n";
      close OUTF;
      $idxr++;
    }
    $idxs++;
  }
}

sub dump_gapStr {
  my($sta,$reg,$cna,$ngaps) = @_;

  open OUTF,">${myDir}/GapStructure${sta}${reg}/${cna}\@GapStructure${sta}${reg}.xml";
  print OUTF "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  print OUTF "<!DOCTYPE DDDB SYSTEM \"../../../DTD/structure.dtd\" []>\n";
  print OUTF "<DDDB>\n";
  $idxg = 1;
  while($idxg <= $ngaps) {
    $idxgRP = $idxg -1;
    $Midxg = $idxg;
#    if($sta =~ /M1/) {$Midxg = $idxg + 1;}
    print OUTF "  <detelem name = \"Gap${idxg}\" classID=\"11007\">\n";
    print OUTF "    <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/${sta}${reg}Cham/lvGasGap\"\n";
    print OUTF "                 support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/${sta}/${reg}/${cna}\"\n";
    print OUTF "                   npath = \"pvGap${Midxg}\" />\n";
#    print OUTF "                   rpath = \"${idxgRP}\" />\n";
    print OUTF "    <detelemref href =\"#GasGap${idxg}\" />\n";
    print OUTF "  </detelem>\n";
    $idxg++;
  }
  $idxg = 1;
  while($idxg <= $ngaps) {
    $idxgRP = $idxg -1;
    print OUTF "  <detelem name = \"GasGap${idxg}\" classID=\"11007\">\n";
    print OUTF "    <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/${sta}${reg}Cham/lvGasGapLayer5\"\n";
    print OUTF "                 support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/${sta}/${reg}/${cna}/Gap${idxg}\"\n";
    print OUTF "                   npath = \"pvGapLayer5\" />\n";
    print OUTF "  </detelem>\n";
    $idxg++;
  }
  print OUTF "</DDDB>\n";
  close OUTF;
}


sub usage {
  my($exit, $message) = @_;

  print STDERR $message if defined $message;
  print STDERR <<INLINE_LITERAL_TEXT; #'

Usage: $0 <options> 
    Script that submits monitoring jobs.

Options:
  -f    :       Structure xml File name
  -nsta :       How Many stations
  -fsta :       Name of stations file
  -nreg :       How many regions PER station
  -freg :       Name of regions file (type: M1freg, M2 freg,..)

Examples:
$0 -f structure.xml -nsta 5 -fsta stations.xml -nreg 4 -freg regions.xml

Comments to <asarti\@lnf.infn.it>.
INLINE_LITERAL_TEXT
#'
  exit($exit) if defined $exit;
}

sub insert_regions {
  my($num,$nreg,$rname) = @_;
  if($rname =~ /(.*)\.xml/) {
    $modname = $1;
  }
  $idxs = 1; $useSpec = 0;
  while($idxs <= $num) {
    $idxr = 1;
    while($idxr <= $nreg) {
      $ngap = 4;
      if($idxs == 1) {$ngap = 2;}
      open OUTF,">${myDir}/dumbM${idxs}R${idxr}$rname";

      print OUTF "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
      print OUTF "<!DOCTYPE DDDB SYSTEM \"../../DTD/structure.dtd\"\n";
      print OUTF " [<!ENTITY Station \"M${idxs}\">\n";
      print OUTF "  <!ENTITY Region  \"R${idxr}\">\n";
      print OUTF "  <!ENTITY gNum  \"${ngap}\">\n";
      print OUTF "  <!ENTITY M${idxs}R${idxr}$modname SYSTEM \"Region${idxr}M${idxs}Structure.xml\">\n";
      if ($useSpec) {
	print OUTF "  <!ELEMENT GasGap EMPTY>\n";
	print OUTF "  <!ATTLIST GasGap chamber CDATA #REQUIRED>\n";
	print OUTF "  <!ATTLIST GasGap Number CDATA #REQUIRED>\n";
	print OUTF "  <!ATTLIST GasGap offset CDATA #REQUIRED>\n";
	print OUTF "  <!ATTLIST GasGap logvol CDATA #REQUIRED>\n";
	print OUTF "  <!ATTLIST GasGap support CDATA #REQUIRED>\n";
      }
      print OUTF " ]>\n";
      print OUTF " \n";
      print OUTF "<DDDB>\n";
      print OUTF "  &M${idxs}R${idxr}$modname;\n";
      print OUTF "</DDDB>\n";

      close OUTF;
      $idxr++;
    }
    $idxs++;
  }
}


sub create_layout {
  my($num,$fname,$nreg,$rname) = @_;
  open OUTF,">ChmbGrid.xml";
  print OUTF "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  print OUTF "<!DOCTYPE DDDB SYSTEM \"../DTD/structure.dtd\" []>\n";
  print OUTF "<DDDB>\n";
  print OUTF "  <catalog name =\"Grid\" > \n";
  print OUTF "    <conditionref href =\"#G1\" />\n";
  print OUTF "    <conditionref href =\"#G2\" />\n";
  print OUTF "    <conditionref href =\"#G3\" />\n";
  print OUTF "    <conditionref href =\"#G4\" />\n";
  print OUTF "    <conditionref href =\"#G5\" />\n";
  print OUTF "    <conditionref href =\"#G6\" />\n";
  print OUTF "    <conditionref href =\"#G7\" />\n";
  print OUTF "    <conditionref href =\"#G8\" />\n";
  print OUTF "    <conditionref href =\"#G9\" />\n";
  print OUTF "    <conditionref href =\"#G10\" />\n";
  print OUTF "  </catalog>\n";

#M1R1 / M1R2
  print OUTF "<!-- M1R1 M1R2 anode -->\n";
  print OUTF "  <condition name =\"G1\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >1</paramVector> \n";
  print OUTF "  </condition>\n";

#starting double readout:

#M2R2
  print OUTF "<!-- M2R2 cathode & wire-->\n";
  print OUTF "  <condition name =\"G4\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684 0.01973684 0.02302632 0.01973684</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >0 1</paramVector> \n";
  print OUTF "  </condition>\n";

#M3R2

  print OUTF "<!-- M3R2 cathode & wire -->\n";
  print OUTF "  <condition name =\"G5\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146 0.02134146 0.02134146 0.01829268 0.02134146 0.02134146 0.02134146</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >0 1</paramVector> \n";
  print OUTF "  </condition>\n";

#M2R1

  print OUTF "<!-- M2R1 cathode & wire -->\n";
  print OUTF "  <condition name =\"G2\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684 0.01973684 0.01973684 0.02631579 0.01973684 0.01973684 0.01973684</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >0 1</paramVector> \n";
  print OUTF "  </condition>\n";

#M3R1

  print OUTF "<!-- M3R1 cathode & wire -->\n";
  print OUTF "  <condition name =\"G3\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.01829268 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.01829268 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.01829268 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.02439024 0.01829268 0.01829268 0.01829268 0.02439024 0.01829268 0.02439024</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >0 1</paramVector> \n";
  print OUTF "  </condition>\n";

#M5R1 M4R1

  print OUTF "<!-- M5R1 M4R1 anode -->\n";
  print OUTF "  <condition name =\"G6\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.125 0.125 0.125 0.125 0.125 0.125 0.125 0.125</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >1</paramVector> \n";
  print OUTF "  </condition>\n";

#M5R2 M4R2

  print OUTF "<!-- M5R2 M4R2 cathode -->\n";
  print OUTF "  <condition name =\"G7\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333 0.08333333</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.25 0.25 0.25 0.25</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >1</paramVector> \n";
  print OUTF "  </condition>\n";


  print OUTF "<!-- M1R3 M2R3 M3R3 cathode -->\n";
  print OUTF "  <condition name =\"G8\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333 0.02083333</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >1</paramVector> \n";
  print OUTF "  </condition>\n";
  print OUTF "<!-- M4R3 M5R3 cathode -->\n";
  print OUTF "  <condition name =\"G9\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >1</paramVector> \n";
  print OUTF "  </condition>\n";

#Concluding with R4 chambers

  print OUTF "<!-- M*R4 anode -->\n";
  print OUTF "  <condition name =\"G10\" classID=\"11094\"> \n";
  print OUTF "    <paramVector name=\"xrd1\" type=\"double\" >0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666 0.0416666</paramVector>  \n";
  print OUTF "    <paramVector name=\"yrd1\" type=\"double\" >0.5 0.5</paramVector> \n";
  print OUTF "    <paramVector name=\"xrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"yrd2\" type=\"double\" >0</paramVector> \n";
  print OUTF "    <paramVector name=\"grrd\" type=\"int\" >0</paramVector> \n";
  print OUTF "  </condition>\n";

  print OUTF "</DDDB>\n";
  close OUTF;
}
