#!/usr/bin/perl
########################################################


$i = 0;

while(<STDIN>) {
  # Skip comments and includes
  next if( /^\/\// || /^#include/ );

  $i++;

  # Extract only unit name and its value after the assignment
  if( /static\s+const\s+HepDouble\s+(\w+)\s+=\s+([\d\w\.\*\s\-\+\/\(\)]*)/ ) {
    $entKey = $1; $entKey =~ s/\s//g;
    $entVal = $2; $entVal =~ s/\s//g;
    #print "entKey: $entKey entVal: $entVal\n";
  } elsif( /#define\s+([\w\d\_\-]*)\s+([\w\d\_\-]*)/ ) {
    $entKey = $1; $entKey =~ s/\s//g;
    $entVal = $2; $entVal =~ s/\s//g;
  }
 
  # If we don't have this unit name yet then
  if( !$entHash{$entKey} ) {
    # store it in the hash with its corresponding value
    $entHash{$entKey} = $entVal;
  }
  
  #last if( $i == 20 );
}

# Hash table of unit names and their values is filled
# Now the real fun begins!

foreach $entKey ( keys %entHash ) {
  next if( !$entKey );
  #print "key:\'$entKey\' val:\'$entHash{$entKey}\'\n";

  $entVal = $entHash{$entKey};
  
  @entMembers   = split( /[\*\/\(\)]/, $entVal );
  @entOperators = split( /[\d\w\.\-\+\s]*/, $entVal );
  
  #print "Operators: $#entOperators >>";
  #foreach $op ( @entOperators ) {
  #  print "$op";
  #}
  #print "<<\n"; #Operators>>$entOperators<<\n";
  
  %outHash = ();
  $lastMem = "";
    
  foreach $mem ( @entMembers ) {
    next if( !$mem );

    next if( $lastMem eq $mem );
    $lastMem = $mem;

    # Look for each occurence of value member in the value string
    @memIndex = ();
    $memIdx = 0;
    $strIdx = 0;
    $strLen = length( $mem );
    
    while( 1 ) {
       
       #print "Before index strIdx: $strIdx memIdx: $memIdx length of \'$mem\': $strLen\n";
       $memIdx = index( $entVal, $mem, $strIdx );
       #print "After index strIdx: $strIdx memIdx: $memIdx length of \'$mem\': $strLen\n";
       
       if( $memIdx != -1 ) {
         push( @memIndex, $memIdx );         
	 $strIdx = $memIdx + $strLen;
       }
       else                {
         last;
       }
       #print "End of loop index strIdx: $strIdx memIdx: $memIdx length of \'$mem\': $strLen\n";
    }
    
    # Add each instance of value member to hash according to its index in the
    # value string
    $lastIdx = -1;
    foreach $idx ( sort {$a <=> $b} ( @memIndex )) {
      #next if( $lastIdx == $idx );
      $lastIdx = $idx;
      $outHash{$idx} = $mem;
      #print "idx: $idx mem: $mem\n";
    }
    #print "----------------------------------------------\n";
    #print "mem: \'$mem\' memIndex: $memIndex entVal: $entVal\n";
  }

  foreach $op ( @entOperators ) {
    next if( !$op );

    $opIdx = 0;
    $strIdx = 0;
    $strLen = length( $op );
    @opIndex = ();
    
    while( 1 ) {
       
       #print "Before index strIdx: $strIdx opIdx: $opIdx length of \'$op\': $strLen\n";
       $opIdx = index( $entVal, $op, $strIdx );
       #print "After index strIdx: $strIdx opIdx: $opIdx length of \'$op\': $strLen\n";
       
       if( $opIdx != -1 ) {
         push( @opIndex, $opIdx );         
	 $strIdx = $opIdx + $strLen;
       }
       else                {
         last;
       }
       #print "End of loop index strIdx: $strIdx opIdx: $opIdx length of \'$op\': $strLen\n";
    }
    
    # Add each instance of operator member to hash according to its index in the
    # value string
    $lastIdx = -1;
    foreach $opidx ( sort {$a <=> $b} ( @opIndex ) ) {
      next if( $lastIdx == $opidx );
      $lastIdx = $opidx;
      $outHash{$opidx} = $op;
      #print "opidx: $opidx operator: $op\n";
    }

    #print "op: \'$op\' opIndex: $opIndex entVal: $entVal\n";
 }

  @outEntVal = ();
  foreach $item ( sort {$a <=> $b} ( keys %outHash ) ) {
    #print "$outHash{$item}";
    push( @outEntVal, $outHash{$item} );
  }
  #print "@outEntVal\n";

  @tempEntVal = ();
  
  #foreach $member (@entMembers) {
  foreach $member (@outEntVal) {
    #print "member: $member\n";
    
    if( $entHash{$member} ) {
      $member = "&".$member.";";
    }

    push( @tempEntVal, $member );

  }
  
  if( $#opIndex >= 0 )            {
    unshift( @tempEntVal, "(" );
    push( @tempEntVal, ")" );
    #print "opIndex: @opIndex\n";
  }

  $#opIndex = -1;

  $entHash{$entKey} = join( '', @tempEntVal );
  
  #print "key:\'$entKey\' val:\'$entHash{$entKey}\'\n";
  #print "----------------------------------------------------------------\n";
}

#foreach $entKey ( sort ( keys %entHash ) ) {
foreach $entKey ( keys %entHash ) {
  next if( !$entKey );
  next if( !$entHash{$entKey} );
  print "<!ENTITY $entKey \"$entHash{$entKey}_clhep_unit_\">\n";
}


