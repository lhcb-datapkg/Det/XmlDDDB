#! /usr/bin/perl -w


@BOXES=qw / Top Bottom Left Right /;
$classID = 9203;
@layerid = qw /X1 U V X2 /;

print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    foreach $BOX ( @BOXES ) {
	print "  station $i\n";
	$FILENAME=">ITT${i}Station${BOX}Structure.xml";
	
#now open the output file:
	open(OUT,$FILENAME);
#write into the output file:
	print OUT qq/<?xml version="1.0" encoding="UTF-8"?>\n/;
	print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/..\/DTD\/structure.dtd">\n/;
	print OUT "<DDDB>\n";
	print OUT "<!-- ***************************************************************** -->\n";
	print OUT "<!-- *            Description of the ITT${i} ${BOX} structure             * -->\n";
	print OUT "<!-- *                                                               * -->\n";
	print OUT "<!-- *                         Author: K. Vervink                    * -->\n";
	print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
	print OUT "<!-- ***************************************************************** -->\n";
	print OUT "\n";
	print OUT qq/  <detelem classID="$classID" name="${BOX}Box" \>\n/;
	print OUT "    <author>K. Vervink</author>\n";
	print OUT "    <version>1.0</version>\n";
	if ($BOX eq "Right" || $BOX eq "Left"){
	    $Orient = "Side";	    
	    print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvITFull${Orient}Box\"\n/;
	    $j = "S";  
	}else{
	    $Orient = "Center";
	    print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvITFull${Orient}Box\" \n/;
	    $j = "C";
	}
        print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Box\"\n/;
	print OUT qq/                  support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}"\n/;
	print OUT qq/                  npath     = "pvIT${BOX}Box"\/>\n/;

        if ($BOX eq "Left") {$BoxNumber ="2";}
        if ($BOX eq "Right")  {$BoxNumber ="1";}   
        if ($BOX eq "Top")  {$BoxNumber ="4";}   
        if ($BOX eq "Bottom") {$BoxNumber ="3";} 

	print OUT qq/    <param name = \"boxID\" type = \"int\"> ${BoxNumber} <\/param> \n/;

	print OUT "\n"; 

	foreach $layer (@layerid){
	    #print OUT qq/    <detelemref href = "Layer\/ITT${i}${BOX}BoxLayer${layer}Structure\.xml\#Layer${layer}"\/>\n/;
            print OUT qq/    <detelemref href = "Layer\/${BOX}${layer}\@ITT${i}Structure\.xml\#Layer${layer}"\/>\n/;
#	    $classID++;
	}	
	print OUT "\n";
	print OUT "  </detelem>\n";
	print OUT "</DDDB>\n";	
	close(OUT);
	
    }
    
}

