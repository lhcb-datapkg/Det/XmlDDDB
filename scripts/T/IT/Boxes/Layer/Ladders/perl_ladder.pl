#! /usr/bin/perl -w


@BOXES=qw / Top Bottom Left Right /;
$classID = 9205;
$laddernr = 0; 
@LAYERID = qw /X1 U V X2 /;
@LADDERID = qw /1 2 3 4 5 6 7 /;

print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    foreach $BOX(@BOXES){
	print "     box $BOX\n";
        foreach $LAYER(@LAYERID){
	    print "        layer $LAYER\n";
	    foreach $LADDER(@LADDERID){
		if ($BOX eq "Right" || $BOX eq "Left") {
		    if ($BOX eq "Right" & $LAYER ne "U") {
			$laddernr = 8- $LADDER;
		    } elsif ($BOX eq "Left" & $LAYER eq "V") {
                        $laddernr = 8- $LADDER;
                    } else {
                        $laddernr = $LADDER;
                    }
                    $Length = "Long";
                    $nSensor = 2; 
                    $CAP = "33"
		} else {
		    if ($BOX eq "Bottom"){
			$laddernr = 8- $LADDER;
		    } else {
		    	$laddernr = $LADDER;
		    }
		    $Length = "Short";
                    $nSensor = 1; 
		    $CAP = "18"
		}
		#$FILENAME=">ITT${i}${BOX}Layer${LAYER}Ladder${laddernr}Structure.xml";
                $FILENAME=">${BOX}${LAYER}L${laddernr}\@ITT${i}Structure.xml";
		#now open the output file:
		open(OUT,$FILENAME);
#write into the output file:
		print OUT qq/<?xml version="1.0" encoding="UTF-8"?>\n/;
		print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/..\/..\/..\/DTD\/structure.dtd">\n/;
		print OUT "<DDDB>\n";
		print OUT "<!-- ***************************************************************** -->\n";
		print OUT "<!-- *   Description of the ITT${i} ${BOX} ${LAYER}-Layer ${LADDER}-Ladder Structure      * -->\n";
		print OUT "<!-- *                                                               * -->\n";
		print OUT "<!-- *                        Author: K. Vervink                     * -->\n";
		print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
		print OUT "<!-- ***************************************************************** -->\n";
		print OUT "\n";
		print OUT qq/  <detelem classID="$classID" name="Ladder${laddernr}">\n/;
		print OUT "    <author>K. Vervink</author>\n";
		print OUT "    <version>1.0</version>\n";
		print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}Ladder\" \n/;
                print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${laddernr}\"\n/;
		print OUT " "x16 . qq/  support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer$LAYER"\n/;
		print OUT qq/                  npath     = "pv${Length}Ladder$LADDER"\/>\n/;
		print OUT qq/    <param name = \"sectorID\" type=\"int\"> $laddernr <\/param> \n/;
		print OUT "\n";

		print OUT qq/    <detelem classID="9210" name="Sensor${laddernr}">\n/;
		print OUT qq/      <geometryinfo lvname  = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}Sensor\" \n/;
		print OUT " "x16 . qq/    support = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer$LAYER\/Ladder${laddernr}"\n/;
		print OUT qq/                    npath   = "pv${Length}Sensor"\/>\n/;

		print OUT qq/      <param name = \"pitch\" type=\"double\"> 0.198*mm <\/param> \n/;
		print OUT qq/      <param name = \"numStrips\" type=\"int\"> 384 <\/param> \n/;
		print OUT qq/      <param name = \"verticalGuardRing\" type=\"double\"> 1.00*mm <\/param> \n/;
	        print OUT qq/      <param name = \"bondGap\" type=\"double\"> 0.150*mm <\/param> \n/;
		print OUT qq/      <param name = \"capacitance\" type=\"double\"> $CAP*picofarad <\/param> \n/;
		print OUT qq/      <param name = \"type\" type=\"string\"> $Length <\/param> \n/;
                print OUT qq/      <param name = \"nSensors\" type=\"int\"> $nSensor <\/param> \n/;
		print OUT "    </detelem> \n";
		print OUT "\n";
		print OUT "  </detelem>\n";
		print OUT "</DDDB>\n";
		close(OUT);
		
	    }
	    
	}
	
    }

}
