#! /usr/bin/perl -w


@BOXES=qw / Top Bottom Left Right /;
$classID = 9202;

print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    $FILENAME=">ITT${i}StationStructure.xml";

#now open the output file:
    open(OUT,$FILENAME);
#write into the output file:
    print OUT qq/<?xml version="1.0" encoding="UTF-8"?>\n/;
    print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/DTD\/structure.dtd">\n/;
    print OUT "<DDDB>\n";
    print OUT "<!-- ***************************************************************** -->\n";
    print OUT "<!-- *            Description of the ITT${i} Station structure          * -->\n";
    print OUT "<!-- *                                                               * -->\n";
    print OUT "<!-- *                        Author: K. Vervink                     * -->\n";
    print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
    print OUT "<!-- ***************************************************************** -->\n";
    print OUT "\n";
    print OUT qq/  <detelem classID=\"$classID\" name=\"Station$i\">\n/;
    print OUT "    <author>K. Vervink</author>\n";
    print OUT "    <version>1.0</version>\n";
    print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvITT${i}Station\" \n/;
    print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}\"\n/;
    print OUT qq/                  support   = \"\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\"\n/;
    print OUT qq/                  npath     = \"pvITT${i}Station\"\/>\n/;
	print OUT qq/    <param name = \"stationID\" type = \"int\"> ${i} <\/param> \n/;

    print OUT "\n";
    foreach $BOX ( @BOXES ){
	print OUT qq/    <detelemref href = "Boxes\/ITT${i}Station${BOX}Structure.xml#${BOX}Box"\/>\n/;
    }

    print OUT "  </detelem>\n";
    print OUT "</DDDB>\n";


		close(OUT);



}
