#! /usr/bin/perl -w

# # lines mean comment
#
# like this you create s.th. like an array called "STATION" with the
# variables T1 T2 T3 inside  (i.e. STATION[1]="T1")
@STATIONS=qw / T1 T2 T3 /;
@BOXES=qw / top bottom left right /;
@LAYERS=qw /f u v b/;   # e.g. f(front) u(stereo) v(stereo) b(back)
@LADDERS=qw / 1 2 3 4 5 6 7 /;


### ATTENTION, here I simply overrwirte the arrays again, to make them smaller
### i.e. treat only one box and one layer in one station...otherwise I have
### too many error messages as I didn't want to initialize all the "example 
### variables $VARIABLE_A..

@STATIONS=qw / T1 T3 /;
@BOXES=qw / top  /;
@LAYERS=qw /f /;   # e.g. f(front) u(stereo) v(stereo) b(back)
@LADDERS=qw / 1 2 3 4 5 6 7 /;




#you can also define s.th. like an array, that is not indexed by a simple
#number, but by anything you like. Like this you can easily and also easy to 
#read, define the things that differ for the different Ladders. E.g. it's 
#position

$VARIABLE_A{"T1","top","f","1"}="0.1";
$VARIABLE_A{"T1","top","f","2"}="0.2";
$VARIABLE_A{"T1","top","f","3"}="0.3";
$VARIABLE_A{"T1","top","f","4"}="0.4";
$VARIABLE_A{"T1","top","f","5"}="0.5";
$VARIABLE_A{"T1","top","f","6"}="0.7";
$VARIABLE_A{"T1","top","f","7"}="0.8";
#
# ... and so on
#
#
$VARIABLE_A{"T3","top","f","1"}="0.13";
$VARIABLE_A{"T3","top","f","2"}="0.24";
$VARIABLE_A{"T3","top","f","3"}="0.35";
$VARIABLE_A{"T3","top","f","4"}="0.46";
$VARIABLE_A{"T3","top","f","5"}="0.57";
$VARIABLE_A{"T3","top","f","6"}="0.78";
$VARIABLE_A{"T3","top","f","7"}="0.89";
#
# ... and so on
#
#
$VARIABLE_A{"T3","bottom","b","7"}="9.3";

#now you can for example loop over all stations, boxes, Layers and ladders
print "now loop over Stations\n";
foreach $STATION(@STATIONS){
    print "  station $STATION\n";
    print "  now loop over Boxes\n";
    foreach $BOX(@BOXES){
	print "     $BOX box\n";
	foreach $LAYER(@LAYERS){
	    print "      now loop over Layers\n";
	    print "         Layer $LAYER\n";
	    print "           now loop over Ladders\n";
	    foreach $LADDER(@LADDERS){
		print "             Ladder $LADDER\n";
#now coose a filename (or a directry) for the XML file, Here the first 
#variables specifiy the ladder, and the last part of the filename, is the
#value of the "VARIABLE_A" for this particular Ladder:
		$FILENAME=">MyFile_${STATION}_${BOX}_${LAYER}_${LADDER}_$VARIABLE_A{${STATION},${BOX},${LAYER},${LADDER}}.xml";

#now open the output file:
		open(OUT,$FILENAME);
#write into the output file:
		print OUT "bla bla and the value of\n";
		print OUT "the VARIABLE_A is $VARIABLE_A{${STATION},${BOX},${LAYER},${LADDER}}\n";
		print OUT " \n";
		print OUT " or write some nice XML code \n";
		print OUT "<MYTAG>  bla bla Ladder ${LADDER} BOX ${BOX}\n";
		print OUT " </MYTAG>\n";
		print OUT " \n";
		print OUT " Everything clear?\n";
		print OUT " \n";
		print OUT "THE USEAGE oF THIS EXAMPLE IS UNDER THE COPYRIGHT\n";
		print OUT " OF Mr. HELGE VOSS :))))) \n";
		print OUT " \n";
		print OUT " \n";
		print OUT " \n";
		  
#close the output file
		close(OUT);
	    }
	}
    }
}





